const WebSocket = require('ws');

const wss = new WebSocket.Server({
	port: 1992
});

let players = []

wss.on('connection', function (ws) {
	ws.on('message', function (textMessage) {
		textMessage = textMessage.toString();
		//console.log(textMessage);
		let message = JSON.parse(textMessage)
		switch (message.operation) {
			case 'register_player':
				registerPlayer(message.body)
				break;
			case 'sync_player_position':
				syncPlayerPosition(message.body)
				break;
			default:
				console.log('Could not handle operation "' + message.operation + '"') 
		}
	});

	function registerPlayer(body) {
		let player_id = players.size
		console.log('Registering player ' + player_id);
		players.push({
			"player_id" : player_id
		});
		sendOperation('spawn_player', {
			"player_id" : player_id
		});
		console.log('Players: ' + JSON.stringify(players));
	}

	function syncPlayerPosition(body) {
		console.log('Synching position of player ' + body.player_id + ', position: ' + body.position.x + ', ' + body.position.y);
		players[body.player_id].position = body.position
		sendOperation('sync_players', {
			"players" : players
		});
	}

	function sendOperation(operation, body) {
		let message = {
			"operation" : operation,
			"body" : body
		}
		wss.clients.forEach(function (client) {
			if (client.readyState === WebSocket.OPEN) {
				client.send(JSON.stringify(message));
			}
		});
	}

});
